// 1. შეადგინეთ მოცემული ინფორმაციისთვის შესაბამისი დატის სტრუქტურა. (შეეცადეთ მონაცემები მაქსიმალურად ოპტიმალურად გადაანაწილოთ და შეინახოთ)
const tourists = [
  {
    name: "mark",
    age: 19,
    placesVisited: ["tbilisi", "london", "rome", "berlin"],
    moneySpent: [120, 200, 150, 140],
  },
  {
    name: "bob",
    age: 21,
    placesVisited: ["miami", "moscow", "vienna", "riga", "kyiv"],
    moneySpent: [90, 240, 100, 76, 123],
  },
  {
    name: "sam",
    age: 22,
    placesVisited: ["tbilisi", "budapest", "warsaw", "vilnius"],
    moneySpent: [118, 95, 210, 236],
  },
  {
    name: "anna",
    age: 20,
    placesVisited: ["new york", "athens", "sydney", "tokyo"],
    moneySpent: [100, 240, 50, 190],
  },
  {
    name: "alex",
    age: 23,
    placesVisited: ["paris", "tbilisi", "madrid", "marsellie", "minsk"],
    moneySpent: [96, 134, 76, 210, 158],
  },
];
// 2. გაარკვიეთ თითოეული სტუდენტისთვის არის თუ არა ის ზრდასრული (21+, ზრდასრულში 21 წელიც იგუისხმება),
// პასუხის მიხედვით შექმენით შესაბამისი key და შეინახეთ შესაბამისი სტუდენტის ინფოში როგორც boolean ტიპის ცვლადი.
for (const tourist of tourists) {
  tourist.isOver21 = tourist.age >= 21;
}
//3. გაარკვიეთ თითოეული სტუდენტისთვის არის თუ არა ის ნამყოფი საქართველოში და პასუხის მიხედვით წინა დავალების ანალოგიურად შექმენით შესაბამისი key და შეინახეთ შესაბამისი სტუდენტის ინფოში
//ამჯერად რაიმე სტინგის სახით (ნამყოფია, არაა ნამყოფი და ა.შ.).
for (const tourist of tourists) {
  for (const place in tourist.placesVisited) {
    tourist.placesVisited[place] === "tbilisi"
      ? (tourist.wasInTbilisi = true)
      : false;
  }
}
//4. დაითვალეთ თითოეული ტურისტისთვის ჯამში რა თანხა დახარჯა მოგზაურობისას.
for (const tourist of tourists) {
  tourist.totalMoneySpent = 0;
  for (const spent of tourist.moneySpent) {
    tourist.totalMoneySpent += spent;
  }
}
//5. დაითვალეთ თითოეული ტურისტისთვის საშუალოდ რა თანხა დახარჯა მოგზაურობისას.
for (const tourist of tourists) {
  tourist.avgMoneySpent = 0;
  for (const spent in tourist) {
    tourist.avgMoneySpent = tourist.totalMoneySpent / tourist.moneySpent.length;
  }
}
//6. გამოავლინეთ ყველაზე "მხარჯველი" ტურისტი, დალოგეთ ვინ დახარჯა ყველაზე მეტი და რამდენი.
let mostMoneySpent = tourists[0];

for (let i = 1; i < tourists.length; i++) {
  mostMoneySpent.totalMoneySpent < tourists[i].totalMoneySpentgit
    ? (mostMoneySpent = tourists[i])
    : mostMoneySpent;
}

for (const tourist of tourists) {
  tourist.totalMoneySpent === mostMoneySpent.totalMoneySpent
    ? (tourist.hasSpentMost = true)
    : false;
}

console.log(tourists);
